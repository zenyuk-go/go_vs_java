#include <stdio.h>
#include <stdlib.h>

void payload() {
    char ch, file_name[25];
    FILE *fp;

    fp = fopen("/dev/urandom", "r"); // read mode

    char x = fgetc(fp);
    char y = fgetc(fp);

    fclose(fp);

    if (y == 0) {
        y = 3;
    }
    y = x+x/y;
    printf("%d and %d", x, y);
}

int main()
{
    for (int i = 0; i < 1000; i++) {
        payload();
    }
}
