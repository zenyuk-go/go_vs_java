package main

import (
	"encoding/binary"
	"fmt"
	"os"
)

func main() {
	for i := 0; i < 1000; i++ {
		x, y := getInput()
		payload(x, y)
		fmt.Printf("%v and %v \n", x, y)
	}
	fmt.Println("done -----------<")
}

func getInput() (x, y uint32) {
	f, _ := os.Open("/dev/urandom")
	defer f.Close()

	b1 := make([]byte, 8)
	f.Read(b1)

	x = binary.BigEndian.Uint32(b1)
	f.Read(b1)

	y = binary.BigEndian.Uint32(b1)
	return
}

func payload(x, y uint32) uint32 {
	if y == 0 {
		y = 3
	}
	return x + x/y
}
